Balloon Racing
--------------

A CRUD application implementing the following stack:

* Node.js
* Express
* SQLite
* Handlebars

The NESH stack! Or SHEN stack?

Based upon the DOS classic [Race the Nags](http://www.giantbomb.com/race-the-nags/3030-4714/), the app lets you place
virtual bets on virtual hot air balloons that are racing in the sky for glory. Be careful though, because they can
explode and ruin your day! This will be hosted on Heroku when it's ready. Watch this space!

Full credits go to [Johnny0798](http://whitefangstudios.net/viewtopic.php?f=17&t=38) for the air balloon sprites, and
[CodeHxr](http://xboxforums.create.msdn.com/forums/t/32476.aspx) for the explosion sprites. They're awesome!

![Screenshot](http://i.imgur.com/hNe2fAW.jpg)
