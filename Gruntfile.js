'use strict';

module.exports = function (grunt) {
	grunt.initConfig({
		express: {
			dev: {
				options: {
					script: 'server.js'
				}
			}
		},

		jshint: {
			all: ['*.js', 'public/js/*.js'],
			options: {
				jshintrc: '.jshintrc',
				force: true
			}
		},

		watch: {
			scripts: {
				files: ['server.js', 'app/**/*.js', 'public/js/**.js',
                    'public/css/*.scss'],
				tasks: ['jshint', 'browserify:spec', 'jasmine',
                    'browserify:dev', 'sass:dev', 'express:dev'],
				options: {
					livereload: true,
					spawn: false
				}
			}
		},

        sass: {
            dev: {
                files: {
                    'public/css/bundle.css': 'public/css/main.scss'
                }
            }
        },

        browserify: {
            dev: {
                options: {
                    debug: true
                },
                files: {
                    'public/main-build.js': ['public/js/main-script.js'],
                    'public/race-build.js': ['public/js/race-script.js'],
                    'public/profile-build.js': ['public/js/profile-script.js']
                }
            },
            spec: {
                options: {
                    debug: true
                },
                files: {
                    'tests/testSpecs.js': ['tests/*.spec.js']
                }
            }
        },

        jasmine: {
            dev: {
                src: ['public/js/bundle.js'],
                options: {
                    specs: 'tests/testSpecs.js'
                }
            }
        }
	});

	grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

	grunt.registerTask('default', ['jshint', 'browserify:spec', 'jasmine',
        'browserify:dev', 'sass:dev', 'express:dev', 'watch']);

    grunt.registerTask('build', ['browserify:dev', 'sass:dev']);
    grunt.registerTask('heroku:development', ['browserify:dev', 'sass:dev']);
};
