// modules
var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var exphbs = require('express-handlebars');
var logger = require('morgan');
var passport = require('passport');
var session = require('express-session');

// load longjohn (extended stack trace in dev mode)
if (process.env.NODE_ENV !== 'production'){
    console.log('loading longjohn!');
    require('longjohn');
}

// setup all the middlewares
app.use(logger('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(session({
    secret: 'ihavenoideawhatiamdoing',
    cookie : {
        maxAge: 3600000 // 1 hour
    }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


// static resources are served in public folder
app.use(express.static(__dirname + '/public'));

// view engine
app.engine('hbs', exphbs({
    extname: '.hbs',
    defaultLayout: 'main',
    helpers: require('./app/helpers.js')
}));
app.set('view engine', 'hbs');

// connect database
require('./app/db');

// attach passport strategies
require('./app/passport.js')(passport);

// routes
require('./app/routes')(app, passport);

//startup
app.listen(8080);
console.log('Server started on port 8080..');
