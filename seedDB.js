/*exported db */
'use strict';

var Utils = require('./public/js/Utils');
var _ = require('lodash');

require('./app/db');

var db = require('./app/db').db;
var Balloon = require('./app/db').Balloon;
var i;
var j;

// create 50 new ballons for bulk insertion
var newBalloons = [];
for(i = 0; i < 50; i++) {
    newBalloons.push({
        id: i+1,
        name: Utils.nameGenerator(),
        color: Utils.getRandomInt(0, 7),
        skill: Utils.getRandomInt(20, 80),
        wins: 0,
        places: 0,
        shows: 0,
        races: 0
    });
}

// simulate races
for(i = 0; i < 200; i++) {
    var results = Utils.simulateRace(_.sample(newBalloons, 6));
    for(j = 0; j < results.length; j++) {
        newBalloons[results[j].id-1] = results[j];
    }
}

// FIXME run this after the DB connection is active
// populate the DB
process.nextTick(function() {
    Balloon.bulkCreate(newBalloons);
});
