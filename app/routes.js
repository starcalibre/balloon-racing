var _ = require('lodash');
var async = require('async');

var Paginator = require('./Paginator');
var Utils = require('../public/js/Utils.js');

var db = require('./db').db;
var Balloon = require('./db').Balloon;
var User = require('./db').User;
var Race = require('./db').Race;

var NUMBER_PER_PAGE = 5;

module.exports = function(app, passport) {

	app.get('/', function(req, res) {
        if(req.isAuthenticated()) {
            res.redirect('/profile');
        }
        else {
            res.render('home');
        }
	});

    app.get('/forget', function(req, res){
        req.session.destroy();
        res.redirect('/');
    });

    app.get('/leaderboard', function(req, res) {
        User.findAll({
            limit: 10,
            order: 'careerWinnings DESC',
            attributes: ['username', 'careerWinnings']
        }).then(function(users) {
            res.render('leaderboard', {
                username: req.user.username,
                leaders: users
            });
        });
    });

    app.get('/profile', function(req, res) {
        if(req.isAuthenticated()) {
            Race
                .count({where: { user_id: req.user.id }})
                .then(function(racesCount, err) {

                    // now we have a count of the races, create the paginator
                    paginator = new Paginator(racesCount, NUMBER_PER_PAGE);

                    // get query parameters, set the page number to the last page
                    // if the number of pages is too great
                    var page = req.query.page || 0;
                    if(page > paginator.numberPages()) {
                        page = paginator.numberPages()
                    }

                    // now make two queries -- get the races according to the paginator,
                    // and the current user info from the DB so we can render the profile
                    async.parallel([
                        function(callback) {
                            Race
                                .findAll({
                                    where: { user_id: req.user.id },
                                    order: 'createdAt DESC',
                                    offset: paginator.pageIndex(page),
                                    limit: paginator.itemsThisPage(page)
                                })
                                .then(function(races, err) {
                                    callback(err, races);
                                })
                        },
                        function(callback) {
                            User
                                .find(req.user.id)
                                .then(function(user, err) {
                                    callback(err, user)
                                })
                        }
                    ], function(err, results) {
                        res.render('profile', {
                            layout: 'profile-layout',
                            username: results[1].username,
                            playerBalance: results[1].playerBalance,
                            careerWinnings: results[1].careerWinnings,
                            races: results[0],
                            page: page,
                            paginator: paginator
                        });
                    });
                })
        }
        else {
            res.redirect('/');
        }

    });

    app.get('/login', function(req, res) {
        res.render('login', {
            message: req.flash('loginMessage')
        });
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));

    // TODO style the flash message!
    app.get('/signup', function(req, res) {
        res.render('signup', {
            message: req.flash('signupMessage')
        });
    });

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash : true
    }));

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/race', function(req, res) {
        // if the user is logged in, get their balance from the db
        if(req.isAuthenticated()) {
            User
                .find(req.user.id)
                .then(function(user) {
                    res.render('race', {
                        layout: 'race-layout',
                        username: user.username,
                        playerBalance: user.playerBalance,
                        user: req.user
                    })
                });
        }
        else {
            // is the guest does not have a balance to work with, give them one
            if(!req.session.playerBalance) {
                req.session.playerBalance = 500;
            }
            res.render('race', {
                layout: 'race-layout',
                playerBalance: req.session.playerBalance
            });
        }
    });

    // update the cookie and send back redirect to be handled by client
    // TODO this should be a PUT route!!!
    // lol
    app.post('/race', function(req, res) {
        if(req.isAuthenticated()) {
            async.parallel([
                function(callback) {
                    User
                        .find(req.user.id)
                        .then(function(user) {
                            user.update({
                                playerBalance: req.body.newBalance
                            });
                            user.increment({
                                careerWinnings: req.body.netWinnings
                            })
                        })
                        .finally(function() {
                            callback()
                        });
                },
                function(callback) {
                    var newRace = Race.build({
                        user_id: req.user.id,
                        balloon: req.body.userBet.name,
                        type: req.body.userBet.type,
                        betAmount: req.body.userBet.amount,
                        netWinnings: req.body.netWinnings
                    });
                    newRace
                        .save()
                        .then(function() {
                            callback();
                        });
                }
            ], function(err) {
                res.redirect('/');
            });
        }
        else {
            req.session.playerBalance = req.body.newBalance;
            res.redirect('/');
        }
    });

    // send back player balance
    app.get('/playerbalance', function(req, res) {
        if(req.isAuthenticated()) {
            User
                .find(req.user.id)
                .then(function(user) {
                    res.json(user.playerBalance);
                });
        }
        else {
            res.json(req.session.playerBalance);
        }
    });

    app.get('/racedata', function(req, res) {
        // get the data for the hot air balloons
        Balloon.findAll({
            order: [[db.fn('RANDOM')]],
            limit: 6
        }).then(function(data) {
            // TODO use a virtual field
            // add odds to document before sending
            for(var i = 0; i < data.length; i++) {
                var races = data[i].dataValues.races;
                var wins = data[i].dataValues.wins;
                var places = data[i].dataValues.places;
                var shows = data[i].dataValues.shows;

                data[i].dataValues.odds = {
                    win: Utils.oddsFromRatio(wins, races),
                    place: Utils.oddsFromRatio(places, races),
                    show: Utils.oddsFromRatio(shows, races)
                }
            }
            res.json(data);
        });
    });

    app.post('/reset', function(req, res) {
        if(req.isAuthenticated()) {
            async.parallel([
                function(callback) {
                    User
                        .update({
                            playerBalance: 1000,
                            careerWinnings: 0
                        }, {
                            where: {
                                id: req.user.id
                            }
                        })
                        .then(function() {
                            callback();
                        })
                },
                function(callback) {
                    Race
                        .destroy({
                            where: {
                                user_id: req.user.id
                            }
                        })
                        .then(function() {
                            callback();
                        });
                }
            ], function(err) {
                res.redirect('/profile');
            });
        }
        else {
            req.session.playerBalance = 500;
            res.redirect('/');
        }
    });

	app.use(function(req, res) {
		res.redirect('/');
	});
};
