/*exported isValidPassword */

var Sequelize = require('sequelize');
var bcrypt = require('bcrypt-nodejs');

// create db instance
var db = new Sequelize(null, null, null, {
    dialect: 'sqlite',
    storage: 'app/store/app_store.db'
});

// TODO save all models as external JS

// add ballon model
var Balloon = db.define('Balloon', {
    name: {
        type: Sequelize.STRING
    },
    color: {
        type: Sequelize.INTEGER
    },
    skill: {
        type: Sequelize.INTEGER
    },
    wins: {
        type: Sequelize.INTEGER
    },
    places: {
        type: Sequelize.INTEGER
    },
    shows: {
        type: Sequelize.INTEGER
    },
    races: {
        type: Sequelize.INTEGER
    }
});

// add user model
var User = db.define('User',{
    username: {
        type: Sequelize.STRING,
        unique: true
    },
    password: {
        type: Sequelize.STRING,
        set: function(password) {
            // hashes the password when saved
            var hashPassword = bcrypt.hashSync(password,
                bcrypt.genSaltSync());
            this.setDataValue('password', hashPassword);
        }
    },
    playerBalance: {
        type: Sequelize.FLOAT
    },
    careerWinnings: {
        type: Sequelize.FLOAT
    }
}, {
    instanceMethods: {
        isValidPassword: function(password) {
            return bcrypt.compareSync(password, this.getDataValue('password'));
        }
    }
});

// add bet model -- each time a signed in user places a bet, save it for history
// TODO add index on user_id
var Race = db.define('Race', {
    user_id: {
        type: Sequelize.INTEGER
    },
    createdAt: {
        type: Sequelize.DATE
    },
    balloon: {
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.STRING
    },
    betAmount: {
        type: Sequelize.FLOAT
    },
    netWinnings: {
        type: Sequelize.FLOAT
    }
});

// one to many, user to race
User.hasMany(Race, { foreignKey: 'user_id' });
Race.belongsTo(User, { foreignKey: 'user_id' });

Balloon.sync({force: false});
User.sync({force: false});
Race.sync({force: false});

module.exports = {
    db: db,
    Balloon: Balloon,
    User: User,
    Race: Race
};
