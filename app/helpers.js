var moment = require('moment');
var Paginator = require('./Paginator');
var Utils = require('../public/js/Utils.js');

module.exports = {
    oddsTable: function() {
        var output = '';
        var i;
        output += '<div class="col-lg-6">';
        for(i = 0; i < 3; i++) {
            output += '<p>' + this.odds[i] + ':1' + '</p>';
        }
        output += '</div>';
        output += '<div class="col-lg-6">';
        for(i = 3; i < 6; i++) {
            output += '<p>' + this.odds[i] + ':1' + '</p>';
        }
        output += '</div>';
        return output;
    },
    renderBalloons: function() {
        var output = '';
        var i;
        for(i = 0; i < this.names.length; i++) {
            output += '<div id="lane' + i + '" class="row balloon-lane" title="' + this.names[i] + '">';
            output += '<div id="balloon' + i + '" class="balloon' + i + '">';
            output += '</div></div>';
        }
        return output;
    },
    formattedPlayerBalance: function() {
        return Utils.formatCurrency(this.playerBalance);
    },
    formattedCareerWinnings: function() {
        return Utils.formatCurrency(this.careerWinnings);
    },
    leaderboardRows: function() {
        var output = [];
        for(var i = 0; i < this.leaders.length; i++) {
            output.push('<tr>');
            output.push('<td>' + (i+1) + '</td>');
            output.push('<td>' + this.leaders[i].username + '</td>');
            output.push('<td>' + Utils.formatCurrency(this.leaders[i]
                .careerWinnings) + '</td>');
            output.push('</tr>');
        }
        return output.join('');
    },
    raceHistoryRows: function() {
        var output = [];
        for(var i = 0; i < this.races.length; i++) {
            var parseDate = moment(this.races[i].createdAt).format('DD-MM-YYYY');
            output.push('<tr>');
            output.push('<td>' + parseDate + '</td>');
            output.push('<td>' + this.races[i].balloon + '</td>');
            output.push('<td>' + this.races[i].type + '</td>');
            output.push('<td>' + Utils.formatCurrency(this.races[i]
                .betAmount) + '</td>');
            output.push('<td>' + Utils.formatCurrency(this.races[i]
                .netWinnings) + '</td>');
            output.push('</tr>');
        }
        return output.join('');
    },
    paginator: function() {
        var output = [];
        output.push('<nav><ul class="pager">');

        if(!this.paginator.hasPrev(this.page)) {
            output.push('<li class="disabled">');
            output.push('<a href="#">Previous</a></li>');
        }
        else {
            output.push('<li>');
            output.push('<a href="/profile?page=' + (+this.page-1) + '">Previous</a></li>');
        }

        if(!this.paginator.hasNext(this.page)) {
            output.push('<li class="disabled">');
            output.push('<a href="#">Next</a></li>');
        }
        else {
            output.push('<li>');
            output.push('<a href="/profile?page=' + (+this.page+1) + '">Next</a></li>');
        }

        output.push('</ul></nav>');
        return output.join('');
    }
};

