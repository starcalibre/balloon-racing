'use strict';

function Paginator(totalItems, itemsPerPage) {
    this.totalItems = totalItems;
    this.itemsPerPage = itemsPerPage;
}

// return the number of pages
Paginator.prototype.numberPages = function() {
    return Math.ceil(this.totalItems/this.itemsPerPage);
};

// return start index of inputed page
Paginator.prototype.pageIndex = function(pageIndex) {
    if(pageIndex < 0 || pageIndex >= this.numberPages()) {
        return null;
    }
    else {
        return pageIndex*this.itemsPerPage;
    }
};

Paginator.prototype.itemsThisPage = function(pageIndex) {
    if(pageIndex === this.numberPages()-1) {
        return this.totalItems % this.itemsPerPage;
    }
    else if(pageIndex >= 0 && pageIndex < this.numberPages()-1) {
        return this.itemsPerPage;
    }
    else {
        return null;
    }
};

Paginator.prototype.hasNext = function(pageIndex) {
    return pageIndex < this.numberPages()-1;
};

Paginator.prototype.hasPrev = function(pageIndex) {
    return pageIndex > 0;
};

module.exports = Paginator;
