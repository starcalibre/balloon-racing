var LocalStrategy = require('passport-local');
var User = require('./db.js').User;

module.exports = function(passport) {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    // TODO dismantle this callback pyramid of doom
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, username, password, done) {
        // TODO handle errors, it's SQL I'm dealing with here I'm probably gonna break it lbr
        User
            .find({
                where: {
                    username: username
                }
            })
            .then(function(user) {
                if(user) {
                    // is a user is found, then the username is taken.
                    // TODO do I need the connect-flash middleware??
                    return done(null, false, req.flash(
                        'signupMessage', 'That username is taken!'
                    ));
                }
                else {
                    // otherwise create the user
                    var newUser = User.build({
                        username: username,
                        password: password,
                        playerBalance: 1000,
                        careerWinnings: 0
                    });
                    newUser
                        .save()
                        .then(function (user) {
                            return done(null, user);
                        })
                        .catch(function (err) {
                            throw(err)
                        });
                }
            })
    }));

    // login strategy
    passport.use('local-login', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, function(req, username, password, done) {
        User
            .find({
                where: {
                    username: username
                }
            })
            .then(function(user) {

                // no user found
                // TODO more flash messages to style here..!
                if(!user) {
                    return done(null, false, req.flash(
                        'loginMessage', 'No user with that name found!'
                    ));
                }

                // invalid password
                if (!user.isValidPassword(password)) {
                    return done(null, false, req.flash(
                        'loginMessage', 'Invalid password.'
                    ));
                }
                return done(null, user);
            })
            .catch(function(err) {
                throw err;
            });
    }));

};




