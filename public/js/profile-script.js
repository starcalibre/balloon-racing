'use strict';

(function() {
    var $resetConfirm = $('#reset-confirm');

    $('#reset-stats').on('click', function() {
        $resetConfirm.modal('show');
    });

    $('#reset-confirm-cancel').on('click', function() {
        $resetConfirm.modal('hide');
    });

    $('#reset-confirm-reset').on('click', function() {
        $.ajax({
            type: 'POST',
            url: '/reset/',
            success: function(xmlHttp) {
                if (xmlHttp.code !== 200) {
                    $resetConfirm.modal('hide');
                    top.location.href = '/profile';
                }
            }
        });
    });
})();
