'use strict';

(function() {
    var Commentary = require('./Commentary.js');
    var Race = require('./Race.js');
    var Utils = require('./Utils');

    var race;
    var playerBalance;
    var raceData;
    var userBet = {
        name: null,
        lane: null,
        amount: null,
        type: null
    };
    var raceResults;

    var $commentaryText = $('#commentary-text');
    var $raceArea = $('#race-area');

    // on script load, send two ajax requests
    // first request gets some balloons and their data from the DB
    // second requests get the players balance from the cookie
    $.when(
        $.ajax({
            type: 'GET',
            url: '/racedata',
            success: function(data) {
                raceData = data;
            }
        }),
        $.ajax({
            type:'GET',
            url: '/playerbalance',
            success: function(data) {
                playerBalance = +data;
            }
        })).done(function() {
            race = new Race(raceData, playerBalance);
        });

    // SETUP DOM ON LOAD
    // add opening commentary remark
    $commentaryText.text(Commentary.beforeRace());

    $('#start-race-button').on('click', function() {
        // add commentary that the race has started
        $commentaryText.text(Commentary.raceStart());

        race.startRace(userBet);

        // disable place bet and start race buttons when race starts
        $('#start-race-button').attr('disabled', 'disabled');
        $('#open-betting-modal').attr('disabled', 'disabled');

    });

    $('#open-betting-modal').on('click', function() {
        $('#betting-menu').modal('show');
    });

    // validation for placing bets
    $('#bet-amount').on('change keyup paste', function() {
        var value = +$('#bet-amount').val();

        // prevent bet from being placed
        // if bet amount exceeds player balance
        if(value > playerBalance) {
            $('#bet-amount-error').removeClass('hidden');
            $('#place-bet-button').attr('disabled', 'disabled');
        }
        else {
            $('#bet-amount-error').addClass('hidden');
            $('#place-bet-button').removeAttr('disabled');
        }
    });

    $('#place-bet-button').on('click', function() {
        var balloon = $('#bet-balloon').val();
        var bet = $('#bet-amount').val();
        var type = $('#bet-type').val();

        userBet.name = balloon;
        userBet.amount = +bet;
        userBet.type = type;

        $('#current-bet-balloon-display').text(balloon);
        $('#current-bet-amount-display').text(Utils.formatCurrency(bet));
        $('#current-bet-type-display').text(type);

        $('#betting-menu').modal('hide');
    });

    // when the race object finishes the race it will fire a
    // resultsAreReady event -- this listener handles that event
    $raceArea.on('resultsAreReady', function(event, data) {
        var $resultsCloseButton = $('#results-close-button');
        var $resultsModalBody = $('#results-modal-body');
        raceResults = data;

        // post new balance to server if bet was placed
        if(raceResults.betPlaced) {
            $.ajax({
                type: 'POST',
                url: '/race',
                data: {
                    newBalance: raceResults.newBalance,
                    winnings: raceResults.winnings,
                    userBet: raceResults.userBet,
                    netWinnings: raceResults.netWinnings
                },
                complete: function(xmlHttp) {
                    // unhide the results button after data posted to the server
                    if (xmlHttp.code !== 200) {
                        $resultsCloseButton.removeClass('hidden');
                        $resultsCloseButton.on('click', function() {
                            $('.balloon').attr('class', 'balloon balloon-empty');
                            $('#race-results').modal('hide');
                            top.location.href = '/race';
                        });
                    }
                }
            });
        }
        // otherwise just open the results modal
        else {
            $resultsCloseButton.removeClass('hidden');
            $resultsCloseButton.on('click', function() {
                $('.balloon').attr('class', 'balloon balloon-empty');
                $('#race-results').modal('hide');
                top.location.href = '/race';
            });
        }


        if(raceResults.explodedBallons === 6) {
            $resultsModalBody
                .append('<p>Whoops, everyone exploded..</p>');
        }
        else {
            var resultList = [];
            resultList.push('<ol>');
            for(var i = 0; i < Math.min(raceResults.winners.length, 3); i++) {
                resultList.push('<li>');
                resultList.push(raceResults.winners[i].name);
                resultList.push('</li>');
            }
            resultList.push('</ol>');
            $resultsModalBody
                .append(resultList.join(''));
        }

        // results
        $resultsModalBody
            .append('<p>' + raceResults.msg + '</p>');
        $resultsModalBody
            .append('<p>' + raceResults.winningsMessage + '</p>');
        $resultsModalBody
            .append('<p>' + raceResults.newBalanceMessage + '</p>');

        $('#race-results').modal('show');
    });

    $raceArea.on('balloonHasExploded', function(event, data) {
        // add commentary for exploded balloon
        $commentaryText
            .text(Commentary.balloonExplodes(data.name));

        // add strikethrough to balloon name when it explodes
        // add 1 as nth-child selector counts from 1
        var number = data.laneNo + 1;
        $('#balloon-list li:nth-child(' + number + ')')
            .css('text-decoration', 'line-through');

        // update odds screen with exploded text
        $('#odds-win-' + data.laneNo)
            .text('Scratched!');
        $('#odds-place-' + data.laneNo)
            .text('Scratched!');
        $('#odds-show-' + data.laneNo)
            .text('Scratched!');
    });

    $('#guest-reset').on('click', function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/reset',
            success: function(xmlHttp) {
                if (xmlHttp.code !== 200) {
                    top.location.href = '/race';
                }
            }
        })
    });
})();
