'use strict';

/*
A collection of utility functions used throughout the codebase. Some might
still not be used at all. They've been placed in their own module as I'll
probably want to share them between the server and the browser.
*/

var _ = require('lodash');
var jStat = require('jStat').jStat;
var numeral = require('numeral');

var Utils = {};

var prefix = [
    'Magnificent',
    'Innocent',
    'Ugly',
    'Rambunctious',
    'Scary',
    'Icky',
    'Unassuming',
    'Auspicious',
    'Bite-sized',
    'Vulgar',
    'Beautiful',
    'Irate',
    'Puzzling',
    'Burly',
    'Infamous',
    'Noisy',
    'Careful',
    'Slim',
    'Grotesque',
    'Secret',
    'Free',
    'Afraid',
    'Motionless'
];

var suffix = [
    'Overcoat',
    'Expletive',
    'Spree',
    'Lonesome',
    'Marketplace',
    'Pelican',
    'Penguin',
    'Fixed-Rate Mortgage',
    'Eggplant',
    'Bitmap',
    'Gunner',
    'Saboteur',
    'Planner',
    'Glider',
    'Do-Gooder',
    'Local Man',
    'Star Gazer',
    'Potato Smasher',
    'Junket',
    'Tin Can',
    'Stealth Bomber',
    'Bombardier',
    'Zeppelin',
    'Fireball',
    'Lizard Wizard',
    'Tulip',
    'Pickle',
    'Lawnmower',
    'Palace'
];

Utils.sampleBeta = function(a, b, min, max) {
    var beta = jStat.beta.sample(a, b);
    return Math.floor(beta * (max - min) + min);
};

Utils.getRandomFloat = function(min, max) {
    return Math.random() * (max - min) + min;
};

Utils.getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
};

Utils.arrayOfNames = function(n) {
    var output = [];
    for(var i = 0; i < n; i++) {
        output.push(Utils.nameGenerator());
    }
    return output;
};

Utils.arrayOfBeta = function(n) {
    var output = [];
    for(var i = 0; i < n; i++) {
        output.push(Utils.sampleBeta(2, 5, 1, 25));
    }
    return output;
};

Utils.nameGenerator = function() {
    var prefixIndex = this.getRandomInt(0, prefix.length-1);
    var suffixIndex = this.getRandomInt(0, suffix.length-1);
    return prefix[prefixIndex] + ' ' + suffix[suffixIndex];
};

Utils.rearrageArray = function(arr, newOrder) {
    if(arr.length !== newOrder.length) {
        throw new Error('arr and newOrder must be same length');
    }
    var rearranged = [];
    for(var i = 0; i < arr.length; i++) {
        rearranged.push(arr[newOrder[i]]);
    }
    return rearranged;
};

Utils.findSortMapping = function(arr) {
    var arrCopy = _.cloneDeep(arr);
    arr.sort();

    var mapping = [];
    for(var i = 0; i < arr.length; i++) {
        mapping.push(arrCopy.indexOf(arr[i]));
    }

    return mapping;
};

Utils.arraySum = function(arr) {
    return arr.reduce(function(prev, curr) {
        return prev + curr;
    });
};

Utils.arrayProportion = function(arr) {
    var sum = Utils.arraySum(arr);
    return _.map(arr, function(value) {
        return value/sum;
    });
};

Utils.greatCommonDivisor = function(a, b) {
    while (b !== 0) {
        var r = b;
        b = a % b;
        a = r;
    }
    return a;
};

Utils.oddsFromRatio = function(a, b) {
    var gcd = Utils.greatCommonDivisor(a, b);
    if(a === 0) {
        return [b, 1];
    }
    else {
        return [b/gcd, a/gcd];
    }
};

Utils.ratioFromPct = function(pct) {
    var a = Math.round(pct*100);
    var b = Utils.greatCommonDivisor(a, 100);
    return [100/b, a/b];
};

Utils.simulateRace = function(data) {
    var copy = _.cloneDeep(data);
    var i;
    for(i = 0; i < copy.length; i++) {
        // randomly change skill
        var tempSkill = -1;
        while (tempSkill < 0 || tempSkill > 100) {
            tempSkill = Math.round(jStat.normal.sample(copy[i].skill, 15));
        }
        copy[i].tempSkill = tempSkill;
        // increment number of races
        copy[i].races++;
    }
    // sort by skill level and update stats
    copy = _.sortBy(copy, 'tempSkill').reverse();
    copy[0].wins++;
    copy[0].places++;
    copy[0].shows++;
    copy[1].places++;
    copy[1].shows++;
    copy[2].shows++;
    // delete temp key
    for(i = 0; i < copy.length; i++) {
        delete copy[i].tempSkill;
    }
    return copy;
};

Utils.formatCurrency = function(number) {
    return numeral(number).format('$0,0.00');
};

module.exports = Utils;
