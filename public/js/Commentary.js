'use strict';
var _ = require('lodash');
var Utils = require('./Utils');
var Commentary = {};

// expression of surprise
var expressionSurprise = [
    'Oustanding! ',
    'By George! ',
    'Remarkable! ',
    'Gee Willickers! '
];

// expression of shock
var expressionShock = [
    'Oh dear! ',
    'Crumbs! ',
    'Heavens to Betsy! ',
    'Golly! '
];

// pre-race text
var beforeRace = [
    'What a jolly good day for flying!',
    'What a marvellous day for flying!',
    'What a brilliant day! I hope none of the balloons explode today!'
];

// crossed halfway line
var crossedHalfway = [
    ' has crossed the halfway line!'
];

// race start
var raceStart = [
    'And they\'re off!',
    'It\'s on! Good luck, you intrepid Aviators!',
    'Pip pip and tally-ho! Let\'s go!'
];

// takes the lead
var takesLead = [
    ' has stormed right ahead of the rest!',
    ' has taken the lead!'
];

// balloon explodes
var balloonExplodes = [
    ' has gone up in flames!',
    ' has been blown to smithereens!',
    ' is on fire! And not in a good way..'
];

var winner = [
    ' is the winner! Good show!',
    ' has taken first place! Well done!'
];

Commentary.balloonExplodes = function(name) {
    return _.sample(expressionShock) + name + _.sample(balloonExplodes);
};

Commentary.balloonFirstPlace = function(name) {
    return _.sample(expressionSurprise) + name + _.sample(winner);
};

Commentary.beforeRace = function() {
    return _.sample(expressionSurprise) + _.sample(beforeRace);
};

Commentary.halfwayLine = function(name) {
    return name + _.sample(crossedHalfway);
};

Commentary.raceStart = function() {
    return _.sample(raceStart);
};

module.exports = Commentary;



