'use strict';
var Balloon = require('./Balloon.js');
var Commentary = require('./Commentary.js');
var _ = require('lodash');
var Utils = require('./Utils.js');

var oddsTable;
var balloonList;
var bettingWindow;

var $commentaryText = $('#commentary-text');
var $raceArea = $('#race-area');

var NUM_BALLOONS = 6;

function Race(raceData, playerBalance) {
    // add the players balance as property of the object
    this.playerBalance = playerBalance;

    // variable to keep new balloon objects
    this.balloonArray = [];

    // the bet inputed by the user
    this.userBet = {};

    // keep track of how many balloons have crossed the finish line
    this.winners = [];

    // keep track of how many ballons have exploded
    this.explodedBallons = [];

    // keep track if a balloon has crossed the halfway point yet
    this.balloonPassedHalfway = false;

    // the width of the racing area when the object is called
    // this is used to determine when balloons have finished the race
    var distanceToWin = $('#race-area').width();

    // iterate through the array and create a new balloon
    for(var i = 0; i < raceData.length; i++) {
        var newBalloon = new Balloon(raceData[i].name, i,
            raceData[i].odds, raceData[i].skill, distanceToWin);
        this.balloonArray.push(newBalloon);
    }

    // add the table that displays the odds for this race
    oddsTable(raceData, 'win');
    oddsTable(raceData, 'place');
    oddsTable(raceData, 'show');
    // add list of racers to panel
    balloonList(raceData);
    // add options to the betting window
    bettingWindow(raceData, playerBalance);

    // listen for event that is triggered each time a balloon
    // reaches the finishing line.
    $raceArea.on('balloonReachedFinishingLine', function(event, data) {

        // commentators announces winner if no other winners in array
        if(!this.winners.length) {
            $commentaryText.text(Commentary.balloonFirstPlace(data.name));
        }

        this.winners.push(data);

        // end the race if three balloons have crossed the finishing line
        if(this.winners.length + this.explodedBallons.length >= NUM_BALLOONS) {
            // trigger race is over event so all the balloons will stop
            $raceArea.trigger('theRaceIsOver');
            // run the function that works out the results of the race
            this.openResultsModal();
        }
    }.bind(this));

    // listen for event that is triggered when a balloon explodes and is dropped
    // from the race
    $raceArea.on('balloonHasExploded', function(event, data) {
        this.explodedBallons.push(data);

        // end the race if all the ballons have exploded
        if(this.winners.length + this.explodedBallons.length >= NUM_BALLOONS) {
            $raceArea.trigger('theRaceIsOver');
            this.openResultsModal();
        }
    }.bind(this));

    // listen for event that is trigged when a balloon has crossed the halfway
    // point
    $raceArea.on('balloonReachedHalfwayLine', function(event, data) {
        if(!this.balloonPassedHalfway) {
            this.balloonPassedHalfway = true;
            $commentaryText.text(Commentary.halfwayLine(data.name));
        }
    }.bind(this));
}

Race.prototype.startRace = function(userBet) {
    // add the bet inputed by the user to the Race objects
    // properties and start all the balloons
    this.userBet = userBet;
    for(var i = 0; i < this.balloonArray.length; i++) {
        this.balloonArray[i].moveBallon();
    }
};

// TODO in fact.. *really* clean this shit up..
Race.prototype.openResultsModal = function() {

    var results = {};
    results.explodedBallons = this.explodedBallons;
    results.winners = this.winners;
    results.userBet = this.userBet;
    results.betPlaced = false;

    // if the balloon the user bet on was one of the winners, it'll have an
    // index greater than -1
    var index = _.pluck(this.winners, 'name').indexOf(this.userBet.name);
    var betType = this.userBet.type;
    var ratio;

    // calculate the winnings and return a message depending on the results
    if(!this.userBet.name) {
        // no bet was placed
        results.msg = 'You didn\'t place a bet in this race..';
        results.winnings = 0;
    }
    else if(betType === 'Win' && index === 0 && index > -1) {
        ratio = this.winners[index].odds.win[0]/
            this.winners[index].odds.win[1];
        results.msg = 'Your balloon won!';
        results.winnings = +this.userBet.amount * ratio;
        results.betPlaced = true;
    }
    else if(betType === 'Place' && index <= 1 && index > -1) {
        ratio = this.winners[index].odds.place[0]/
            this.winners[index].odds.place[1];
        results.msg = 'You balloon placed!';
        results.winnings = +this.userBet.amount * ratio;
        results.betPlaced = true;
    }
    else if(betType === 'Show' && index <= 2 && index > -1) {
        ratio = this.winners[index].odds.show[0]/
            this.winners[index].odds.show[1];
        results.msg = 'Your balloon showed!';
        results.winnings = +this.userBet.amount * ratio;
        results.betPlaced = true;
    }
    else {
        results.msg = 'You didn\'t win anything!';
        results.winnings = 0;
        results.betPlaced = true;
    }

    // update user balance
    results.newBalance = this.playerBalance - this.userBet.amount +
        results.winnings;

    // winnings less the original bet
    results.netWinnings = results.winnings - this.userBet.amount;
    if(results.netWinnings > 0) {
        results.winningsMessage = 'You won ' +
            Utils.formatCurrency(results.netWinnings) + '!';
    }
    else {
        results.winningsMessage = 'You lost ' +
            Utils.formatCurrency(-results.netWinnings);
    }

    results.newBalanceMessage = 'Your balance is now ' +
        Utils.formatCurrency(results.newBalance) + '!';

    // fire an event to send the results out
    $raceArea.trigger('resultsAreReady', results);
};

oddsTable = function(raceData, type) {
    var output = '';
    var i;
    var tag;

    var odds = _.pluck(_.pluck(raceData, 'odds'), type);

    output += '<div class="col-lg-6">';
    for(i = 0; i < 3; i++) {
        tag = '<p id="odds-' + type + '-' + i + '">';
        output += tag + odds[i][0] + ':' + odds[i][1] + '</p>';
    }
    output += '</div>';
    output += '<div class="col-lg-6">';
    for(i = 3; i < 6; i++) {
        tag = '<p id="odds-' + type + '-' + i + '">';
        output += tag + odds[i][0] + ':' + odds[i][1] +'</p>';
    }
    output += '</div>';

    $('#' + type + '-odds').append(output);
};

balloonList = function(raceData) {
    var output = '';
    for(var i = 0; i < raceData.length; i++) {
        output += '<li>';
        output += raceData[i].name;
        output += '</li>';
    }

    $('#balloon-list').append(output);
};

bettingWindow = function(raceData, playerBalance) {
    var output = '';
    for(var i = 0; i < raceData.length; i++) {
        output += '<option value="' + raceData[i].name + '">';
        output += raceData[i].name;
        output += '</option>';
    }

    $('#bet-player-balance').text(Utils.formatCurrency(playerBalance));
    $('#display-player-balance').text(Utils.formatCurrency(playerBalance));
    $('#bet-amount').attr('');
    $('#bet-balloon').append(output);
};

module.exports = Race;
