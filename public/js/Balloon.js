'use strict';

var Utils = require('./Utils.js');
var $raceArea = $('#race-area');

function Balloon(name, laneNo, odds, skill, winDistance) {
    this.name = name;
    this.laneNo = laneNo;
    this.odds = odds;
    this.skill = skill;

    this.distanceToWin = winDistance;
    this.halfwayDistance = winDistance/2;
    this.distanceTravelled = 0;

    this.$laneSelector = $('#lane' + this.laneNo);
    this.$balloonSelector = $('#balloon' + this.laneNo);

    this.$laneSelector
        .attr('title', this.name);
    this.$balloonSelector
        .removeClass('balloon-empty')
        .addClass('balloon' + this.laneNo);

    $raceArea.on('theRaceIsOver', function() {
        this.$balloonSelector.stop();
    }.bind(this));

    this.$balloonSelector.on('explodeBalloon', function() {
        this.explode();
    }.bind(this));
}

Balloon.prototype.moveBallon = function() {
    // will the balloon explode?
    var explodeChance = 0.06 - 0.05*(this.skill/100);
    var willExplode = explodeChance > Utils.getRandomFloat(0, 1);

    if(willExplode) {
        this.explode();
    }
    else {
        // if the balloon is lucky enough not to explode, it moves!

        // calculate distance balloon will travel
        var distance = Utils.sampleBeta(2+(this.skill/100),
            3-(this.skill/100), 50, 200);

        // calculate the speed (ie animation duration)
        var duration = Utils.sampleBeta(4-2*(this.skill/100),
            2+(this.skill/100), 400, 800);

        // fire event when balloon crosses halfway point
        if(this.distanceTravelled + distance > this.halfwayDistance) {
            $raceArea
                .trigger('balloonReachedHalfwayLine', this);
        }

        // take final move once balloon reaches finishes line
        if(this.distanceTravelled + distance > this.distanceToWin) {
            this.$balloonSelector.animate({
                'left': this.distanceToWin + 'px'
            }, duration, function() {
                $raceArea
                    .trigger('balloonReachedFinishingLine', this);
            }.bind(this));
        }
        else {
            this.$balloonSelector.animate({
                'left': '+=' + distance + 'px'
            }, duration, function() {
                this.distanceTravelled += distance;
                this.moveBallon();
            }.bind(this));
        }
    }
};

Balloon.prototype.explode = function() {
    // add explosion animation
    this.$balloonSelector
        .stop()
        .removeClass('balloon' + this.laneNo)
        .addClass('explosion');
    // trigger event
    $raceArea
        .trigger('balloonHasExploded', this);
};

module.exports = Balloon;
