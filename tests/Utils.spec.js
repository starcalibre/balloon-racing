var Utils = require('../public/js/Utils.js');

describe('Utils', function() {
    describe('getRandomFloat', function() {
        it('should return float in range of [0, 5)', function() {
            var number = Utils.getRandomFloat(0, 5);
            expect(number >= 0).toBeTruthy();
            expect(number).toBeLessThan(5);
        });
    });

    describe('getRandomInt', function() {
        it('should return an integer in range of {0, .., 5}', function() {
            var number = Utils.getRandomInt(0, 5);
            expect(number >= 0).toBeTruthy();
            expect(number <= 5).toBeTruthy();
        });
    });

    describe('rearrageArray', function() {
        it('should return the correctly rearranged array', function() {
            var rearrange = Utils.rearrageArray(['a', 'b', 'c', 'd'],
                [1, 3, 2, 0]);
            expect(rearrange).toEqual(['b', 'd', 'c', 'a']);
        });

        it('should throw an error if new index not same length as input array', function() {
            expect(function() {
                Utils.rearrageArray(['a','b','c'], [0, 1])
            }).toThrow(new Error('arr and newOrder must be same length'));
        });
    });

    describe('findSortMapping', function() {
        it('should return the correct sort mapping', function() {
            var mapping = Utils.findSortMapping([0, 4, 5, 1]);
            expect(mapping).toEqual([0, 3, 1, 2]);
        });
    });

    describe('arraySum', function() {
        it('should return the correct array sum', function() {
            var sum = Utils.arraySum([1, 4, 7, 5, 0]);
            expect(sum).toEqual(17)
        });
    });

    describe('arrayProportion', function() {
        it('should return the correct proportional values', function() {
            var proportion = Utils.arrayProportion([0, 1, 2, 3, 4]);
            expect(proportion).toEqual([0, 0.1, 0.2, 0.3, 0.4]);
        });
    });

    describe('greatestCommonDivisor', function() {
        it('should find the correct GCD', function() {
            expect(Utils.greatCommonDivisor(12, 3)).toEqual(3);
            expect(Utils.greatCommonDivisor(11, 3)).toEqual(1);
        });

        it('should handle second parameter greater than the first', function() {
            expect(Utils.greatCommonDivisor(3, 12)).toEqual(3);
            expect(Utils.greatCommonDivisor(3, 11)).toEqual(1);
        });
    });

    describe('oddsFromPct', function() {
        it('should find the correct odds', function() {
            expect(Utils.ratioFromPct(0.25)).toEqual([4, 1]);
            expect(Utils.ratioFromPct(0.05)).toEqual([20, 1]);
        });
    });

    describe('formatCurrency', function() {
        it('should correctly format currency', function() {
            expect(Utils.formatCurrency(101.5632)).toEqual('$101.56');
        });
    });
});

