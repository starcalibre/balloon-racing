var Paginator = require('../app/Paginator');

describe('Paginator', function() {
    var paginator10_4 = new Paginator(10, 4);
    var paginator6_5 = new Paginator(6, 5);
    var paginator7_10 = new Paginator(7, 10);

    describe('numberPages', function() {
        it('should return correct number of pages', function() {
            expect(paginator10_4.numberPages()).toEqual(3);
            expect(paginator6_5.numberPages()).toEqual(2);
            expect(paginator7_10.numberPages()).toEqual(1);
        })
    });

    describe('pageIndex', function() {
        it('should return correct index for first page', function() {
            expect(paginator10_4.pageIndex(0)).toEqual(0);
            expect(paginator6_5.pageIndex(0)).toEqual(0);
            expect(paginator7_10.pageIndex(0)).toEqual(0);
        });

        it('should return correct index for second page', function() {
            expect(paginator10_4.pageIndex(1)).toEqual(4);
            expect(paginator6_5.pageIndex(1)).toEqual(5);
        });

        it('should return correct index for third page', function() {
            expect(paginator10_4.pageIndex(2)).toEqual(8);
        });

        it('should return null if index less than zero', function() {
            expect(paginator10_4.pageIndex(-1)).toBeNull();
        });

        it('should return null if index too great', function() {
            expect(paginator10_4.pageIndex(4)).toBeNull();
            expect(paginator6_5.pageIndex(2)).toBeNull();
            expect(paginator7_10.pageIndex(1)).toBeNull();
        });
    });

    describe('itemsThisPage', function() {
        it('should return correct number of for the first page', function() {
            expect(paginator10_4.itemsThisPage(0)).toEqual(4);
            expect(paginator6_5.itemsThisPage(0)).toEqual(5);
            expect(paginator7_10.itemsThisPage(0)).toEqual(7);
        });

        it('should return correct number of for the second page', function() {
            expect(paginator10_4.itemsThisPage(1)).toEqual(4);
            expect(paginator6_5.itemsThisPage(1)).toEqual(1);
        });

        it('should return correct number of for the third page', function() {
            expect(paginator10_4.itemsThisPage(2)).toEqual(2);
        });

        it('should return null if index less than zero', function() {
            expect(paginator10_4.itemsThisPage(-1)).toBeNull();
        });

        it('should return null if index too great', function() {
            expect(paginator10_4.itemsThisPage(4)).toBeNull();
            expect(paginator6_5.itemsThisPage(2)).toBeNull();
            expect(paginator7_10.itemsThisPage(1)).toBeNull();
        });
    });

    describe('hasNext', function() {
        it('should be true if it has a next page', function () {
            expect(paginator10_4.hasNext(1)).toBeTruthy();
            expect(paginator6_5.hasNext(0)).toBeTruthy();
        });

        it('should be false if it does not have a next page', function () {
            expect(paginator10_4.hasNext(3)).toBeFalsy();
            expect(paginator6_5.hasNext(2)).toBeFalsy();
            expect(paginator7_10.hasNext(0)).toBeFalsy();
        });
    });

    describe('hasPrev', function() {

        it('should be true if the paginator has a prev page', function() {
            expect(paginator10_4.hasPrev(3)).toBeTruthy();
            expect(paginator6_5.hasPrev(2)).toBeTruthy();
        });

        it('should be false if it does not have a prev page', function() {
            expect(paginator10_4.hasPrev(0)).toBeFalsy();
            expect(paginator6_5.hasPrev(0)).toBeFalsy();
            expect(paginator7_10.hasPrev(0)).toBeFalsy()

        });
    });
});
